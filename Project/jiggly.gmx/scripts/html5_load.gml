var rm = argument[0]; 
var temp_obj1 = instance_create(0,0,oNPC);
var temp_obj2 = instance_create(0,0,oDoor);
var temp_obj3 = instance_create(0,0,oChest);
//var rm = room_names[? room];
ini_open(room_names[?rm]+".lv_html5");
var i = 0;
var pam;
pam[0,0] = 0;
while true {
    if !ini_section_exists("object_"+string(i)) {
        //show_debug_message("Section object_"+string(i)+" does not exist, breaking");
        break;
    }
    var obj = asset_get_index(ini_read_string("object_"+string(i),"name","oChar"));
    var xp = ini_read_real("object_"+string(i),"x",irandom(area_width-16));
    var yp = ini_read_real("object_"+string(i),"y",irandom(area_height-16));
    //var inst = instance_create(xp,yp,obj);
    var inst = room_instance_add(rm,xp,yp,obj);
    var ro = room_objects[? rm];
    ro[? room_names[?rm]+string(inst)] = i;
    if obj == oChest or obj == oNPC or obj == oDoor {
        create_param_array(obj,i,pam);
    }
    //show_debug_message("Added instance of "+object_get_name(obj)+" at "+string(xp)+","+string(yp)+" to room "+room_names[?rm]);
    i++;
}
param_map[? room_names[?rm]] = pam;
ini_close();
instance_destroy(temp_obj1);
instance_destroy(temp_obj2);
instance_destroy(temp_obj3);
