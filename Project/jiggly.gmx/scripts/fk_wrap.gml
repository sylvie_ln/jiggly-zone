var wrap;
wrap = false;

priority_bonus = 0;

if xp < oFlowerKitty.game_x {
    xp += oFlowerKitty.game_w;
    wrap = true; 
}
if xp > oFlowerKitty.game_x+oFlowerKitty.game_w {
    xp -= oFlowerKitty.game_w;
    wrap = true;
}
if yp < oFlowerKitty.game_y {
    yp += oFlowerKitty.game_h;
    wrap = true;
}
if yp > oFlowerKitty.game_y+oFlowerKitty.game_h {
    yp -= oFlowerKitty.game_h;
    wrap = true;
}

if wrap {
    priority_bonus = bonus;
}
