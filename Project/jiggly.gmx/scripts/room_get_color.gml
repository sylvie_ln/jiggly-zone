var col = string_letters(room_names[? argument[0]]);
if room_get_alt(argument[0]) {
    return string_copy(col,1,string_length(col)-1);
} else {
    return col;
}
