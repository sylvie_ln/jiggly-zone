active = false;

kp[ki.enter] = 0;
kp[ki.start] = 0;

instance_activate_all();

audio_stop_sound(bgmConfigMenu);

view_xview[0] = temp_xview;
view_yview[0] = temp_yview;

ini_open(options_file);
var i;
ini_write_real("version","version",options_file_version);
for(i=0;i<options.last;i++) {
    ini_write_real("options",string(i),sub_option[i]);
    //show_message(string(i)+","+string(sub_option[i]));
}
for(i=0;i<sub_options.last;i++) {
    if sub_option_type[i] == sub_types.multi {
        ini_write_real("sub_options",string(i),sub_option_value[i]); 
    }
}
for(i=0;i<ki.last;i++) {
    var k = oInput.key_map[i];
    if is_array(k) {
        ini_write_real("gamepad_id",string(i),k[0])
        ini_write_real("key",string(i),k[1]);
        ini_write_real("gamepad_axis",string(i),k[2]);
        ini_write_string("gamepad_name",string(i),k[3]);
    } else {
        ini_write_real("key",string(i),k);
    }
}
ini_write_real("key","enter_backup",oInput.enter_backup);
ini_write_real("key","menu_backup",oInput.menu_backup);
ini_close();
