audio_stop_sound(global.bgm);
if argument_count == 1 {
    global.bgm = audio_play_sound(argument[0],0,true);
} else if argument_count == 2 {
    global.bgm = audio_play_sound(argument[0],0,argument[1]);
}
audio_sound_gain(global.bgm,global.bgm_volume,0);

/*
if argument[0] == bgmPink {
    audio_sound_pitch(global.bgm,0.9);
}
*/

