var key = argument[0];
if is_array(key) {
    //var i = gpa_id(key);
    for(var i=0;i<gamepad_get_device_count();i++) {
        if !gamepad_is_connected(i) { continue; }
        switch(gpa_button(key)) {
            case gp_axislh:
            case gp_axisrh:
            case gp_axislv:
            case gp_axisrv:
            if sign(gamepad_axis_value(i,gpa_button(key))) == gpa_axis_dir(key) {
                return true;
            }
        }
        if !is_axis_constant(gpa_button(key)) {
            if gamepad_button_check(i,gpa_button(key)) {
                return true;
            }
        }
    }
    return false;
} else {
    return keyboard_check(key);
}
