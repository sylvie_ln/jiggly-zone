var area_color = c_white;
if argument_count == 0 {
    var rm = room_get_color(room);   
} else {
    var rm = argument[0];
}
switch(rm) {
    case "pink": area_color = c_fuchsia; break;
    case "red": area_color = c_red; break;
    case "orange": area_color = c_orange; break;
    case "yellow": area_color = c_yellow; break;
    case "green": area_color = c_lime; break;
    case "blue": area_color = c_blue; break;
    case "purple": area_color = make_color_rgb(191,0,255); break;
}
return area_color;
