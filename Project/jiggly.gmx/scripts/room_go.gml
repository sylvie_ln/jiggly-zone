if room != rmInit { global.previous_room = room; }
var rm = room_indices[? argument[0]];
if room != rm {
    room_goto(rm);
}
