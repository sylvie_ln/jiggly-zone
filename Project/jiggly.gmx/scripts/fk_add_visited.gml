var i;
var check = false;
for(i=0;i<ds_list_size(visited);i++) {
    var p = visited[|i];
    var xp = p[0];
    var yp = p[1];
    if xp == argument0 and yp == argument1 {
        check = true;
        break;
    }
}

priority = argument2+priority_bonus;
if !check {
    ds_list_add(visited,triple(argument0,argument1,priority));
}
