global.fullscreen = (sub_option[options.fullscreen] == sub_options.fullscreen_yes);
global.scaling = sub_option[options.scaling];
global.interpolation = (sub_option[options.interpolation] == sub_options.interpolation_yes);
global.frameskip = sub_option[options.frameskip];
global.scaling_x = sub_option_value[sub_options.scaling_x];
global.bgm_volume = sub_option_value[sub_options.bgm_volume_adjust]/100;
global.sfx_volume = sub_option_value[sub_options.sfx_volume_adjust]/100;
global.volume = sub_option_value[sub_options.volume_adjust]/100;
oInput.key_repeat_delay = ceil(sub_option_value[sub_options.key_repeat_delay_adjust]*(room_speed/1000));
oInput.key_repeat_speed = ceil(sub_option_value[sub_options.key_repeat_speed_adjust]*(room_speed/1000));
global.deadzone = sub_option_value[sub_options.gamepad_deadzone_adjust]/100;


if argument_count > 0 {
    do_scaling = argument[0];
} else {
    do_scaling = true;
}

if option_changed {
    if global.fullscreen {
        window_set_fullscreen(true);
    } else {
        window_set_fullscreen(false);
    }
    
    if do_scaling {
        update_scaling();
    }
    
    if global.interpolation {
        texture_set_interpolation(true);
    } else {
        texture_set_interpolation(false);
    }
}

audio_sound_gain(global.bgm,global.bgm_volume,0);
audio_set_master_gain(0,global.volume);
