var object = argument[0];
var target = argument[1];
if instance_number(target) == 0 {
    return 0;
}
repeat(adjacent_attempts) {
    var b = instance_find(target,irandom(instance_number(target)-1));
    var xp = b.x;
    var yp = b.y;
    var xoff = 0;
    var yoff = 0;
    var spike_dir = irandom(3);
    if direction_memory {
        if random(1) < direction_memory_probability {
            spike_dir = global.last_adjacent_dir;
        }
    }
    var spike_dir_off = 0;
    var spike = noone;
    repeat(4) {
        switch((spike_dir+spike_dir_off) mod 4) {
            case 0:
                if (1 & direction_locks) == 1 { continue; }
                xoff = block_size; yoff = 0; break;
            case 1: 
                if (2 & direction_locks) == 2 { continue; }
                xoff = 0; yoff = -block_size; break;
            case 2: 
                if (4 & direction_locks) == 4 { continue; }
                xoff = -block_size; yoff = 0; break;
            case 3: 
                if (8 & direction_locks) == 8 { continue; }
                xoff = 0; yoff = block_size; break;
        }
        if !pm(xp+xoff,yp+yoff,oLevelObject) {
            spike = instance_create(xp+xoff,yp+yoff,object);
            var adjacent_count = 0;
            with spike {
                var bs = other.block_size;
                if pmr(0,bs,target) { adjacent_count++; }
                if pmr(0,-bs,target) { adjacent_count++; }
                if pmr(bs,0,target) { adjacent_count++; }
                if pmr(-bs,0,target) { adjacent_count++; }
                if other.check_diagonals {
                    if pmr(bs,bs,target) { adjacent_count++; }
                    if pmr(bs,-bs,target) { adjacent_count++; }
                    if pmr(-bs,bs,target) { adjacent_count++; }
                    if pmr(-bs,-bs,target) { adjacent_count++; }
                }
            }
            if adjacent_count > adjacent_count_max {
                with spike { instance_destroy(); }
            } else {
                global.last_adjacent_dir = spike_dir;
                break;
            }
        } else {
            spike_dir_off++;
        }
    }
    if spike != noone {
        break;
    }
}
