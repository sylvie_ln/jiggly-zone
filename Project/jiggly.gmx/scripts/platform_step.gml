//show_debug_message("Start Step!");
if hv == undefined { hv = 0; }
if vv == undefined { vv = 0; }
if global.editing_text { exit; }

//show_debug_message("Check door timer");
if door_timer > 0 {
    door_timer--;
    visible = false;
    if door_timer <= 0 {
        visible = true;
        room_go(door_room);
        save_game();
    }
    exit;
}

//show_debug_message("Check for jump-to door");
if jump_to_door != -1 {
    with oDoor {
        if id == other.last_door { continue; }
        var door_id = params[0,door_params.door_id];
        if door_id == other.jump_to_door {
            other.x = x;
            other.y = y;
            other.last_door = id;
            entered = true;
            door_entered[? room_names[? room]+"_"+string(params[0,door_params.door_id])] = true;
            break;
        }
    }
    ghost = false;
    grav_dir = 1;
    jump_to_door = -1;
    jumped_to_door = true;
    sfx_play(sfxDoorLeave);
}

//show_debug_message("Check for if I should die");
if !dead {
    if (pmr(0,0,oDeath) and !(global.spike_upgrade and ghost) and !global.invincible)
    or (grav_dir == 1  and y > room_height)
    or (grav_dir == -1 and y < 0) 
    or kp[ki.retry] {
        dead = true;
        sfx_play(sfxOw);
        hv = 0;
        vv = 0;
        jump_count = 0;
        if kp[ki.retry] {
            pressed_retry = true;
            checkpoint = false;
        }
    }
}

//show_debug_message("Check if I am dead");
if dead {
    if kp[ki.retry] {
        pressed_retry = true;
        checkpoint = false;
    }
    image_xscale = 1;
    sprite_index = sDie;
    death_timer++;
    if death_timer >= explode_time {
        visible = false;
    }
    if death_timer >= death_time {
        if pressed_retry or !checkpoint {
            checkpoint = false;
            if !instance_exists(last_door) {
                x = xstart;
                y = ystart;
                xr = 0;
                yr = 0;
            } else {
                x = last_door.x;
                y = last_door.y;
                xr = 0;
                yr = 0;
            }
            grav_dir = 1;
        } else {
            grav_dir = checkpoint_grav;
            x = checkpoint_x;
            y = checkpoint_y;
            xr = 0;
            yr = 0;
            hdir = checkpoint_hdir;
        }
        ghost = false;
        with oB16 {
            solid = !other.ghost;
        }
        pressed_retry = false;
        dead = false;
        death_timer = 0;
        visible = true;
        sprite_index = sS;
        sfx_play(sfxWa);
        hv = 0;
        vv = 0;
    }
    exit;
} 

//show_debug_message("Check if I'm over a door");
if !jumped_to_door {
    with ipr(0,0,oDoor) {
        if locked { continue; }
        other.last_door = id;
        var final_treasure = false;
        if instance_exists(oTreasure) and oTreasure.treasure == 24 {
            final_treasure = true;
        }
        if kp[ki.up] and !final_treasure { //or kp[ki.down] {
            var door_id = params[0,door_params.door_id];
            if kp[ki.up] {
                entered = true;
                door_entered[? room_names[? room]+"_"+string(params[0,door_params.door_id])] = true;
                var target_room = 
                room_colors[params[0,door_params.color]]+string(params[0,door_params.number]);
                if room_get_alt(room) {
                    target_room += "a";
                }
                other.jump_to_door = door_id;
                other.door_timer = other.door_time;
                other.door_room = target_room;
                sfx_play(sfxDoorEnter);
            } else if false and kp[ki.down] and global.door_upgrade {
                entered = true;
                door_entered[? room_names[? room]+"_"+string(params[0,door_params.door_id])] = true;
                var name = room_names[? room];
                if room_get_alt(room) {
                    var target_room = 
                    string_copy(name,1,string_length(name)-1);
                } else {
                    var target_room = name+"a";
                }
                other.jump_to_door = door_id;
                room_go(target_room);
                save_game();
            }
        }
    }
}
jumped_to_door = false;

//show_debug_message("Check if I'm over a chest");
with ipr(0,0,oChest) {
    if kp[ki.up] and !open {
        sfx_play(sfxChestOpen);
        open = true;
        if params[0,chest_params.num] != 999 {
            chest_opened[? room_names[? room]+"_"+string(params[0,chest_params.num])] = true;
        }
        var treasure = params[0,chest_params.treasure];
        if treasure < treasure_num {
            global.treasure[treasure] = true;
            set_upgrade_vars();
            var t = instance_create(x,y,oTreasure);
            t.treasure = treasure;
            t.image_index = treasure;
        } else {
            var m = room_get_color_index(room);
            for(i=0;i<global.shard_total;i++) {
                if !global.medal[m,i] {
                    global.medal[m,i] = true;
                    break;
                }
            }
            var t = instance_create(x+8,y+8,oTreasure);
            t.shard = i;
            t.treasure = treasure;
            t.sprite_index = sMedal;
            t.image_index = i;
        }
        save_game();
    }
}

//show_debug_message("Check if I'm over an NPC");
with ipr(0,0,oNPC) {
    if kp[ki.up] {
        talked = true;
        npc_talked[? room_names[? room]+"_"+string(params[0,npc_params.num])] = true;
        switch(params[0,npc_params.c_base]) {
            case 11: sfx_play(sfxNPCPink); break;
            case 0: 
            case 12: sfx_play(sfxNPCRed); break;
            case 1: sfx_play(sfxNPCOrange); break;
            case 2: sfx_play(sfxNPCYellow); break;
            case 3:
            case 4:
            case 5: sfx_play(sfxNPCGreen); break;
            case 6:
            case 7:
            case 8: sfx_play(sfxNPCBlue); break;
            case 9:
            case 10: sfx_play(sfxNPCPurple); break;
        }
        display_text = true;
        var this_id = id;
        with oNPC {
            if id == this_id { continue; }
            display_text = false;
        }
        display_timer = display_time;
        save_game();
    }
}

//show_debug_message("Check if there's a floaty treasure onscreen");
if instance_exists(oTreasure) {
    exit;
}


//show_debug_message("Handle horizontal input");
hdir = 0;
if kh[ki.left] {
    hdir = -1;   
}
if kh[ki.right] {
    hdir = 1;
}
if kh[ki.left] and kh[ki.right] {
    if khl[ki.left] <= khl[ki.right] {
        hdir = -1;
    } else {
        hdir = 1;
    }
}
//show_debug_message("hdir="+string(hdir));

//show_debug_message("Handle horizontal movement");
if hdir != 0 {
    if abs(hv+(acc*hdir)) > hmax {
        if hv >= 0 {
            hv = max(hv,hmax);
        } else {
            hv = min(hv,-hmax);
        }
    } else {
        hv += acc*hdir;
    }
}

//show_debug_message("hv="+string(hv));


//show_debug_message("Handle friction");
if hdir == 0 {
    var s = sign(hv);
    hv -= (fric*s);
    if sign(hv) != s {
        hv = 0;
    }
}
//show_debug_message("hv="+string(hv));


//show_debug_message("Reset jump count if on ground");
if on_ground() and jump_count != 0 {
    jump_count = 0;
} else if !on_ground() {
    jump_count = max(jump_count,1);
}

//show_debug_message("Handle gravity");
if !on_ground() {
    if vv+grav*grav_dir > vmax {
        vv = max(vv,vmax);
    } else {
        vv += grav*grav_dir;
    }
}
//show_debug_message("vv="+string(vv));


//show_debug_message("Handle jumps");
if kp[ki.jump] and (on_ground() or (global.jump_upgrade and (jump_count < jump_max))) {
    vv = -jmp[jump_count]*grav_dir;
    sfx_play(asset_get_index("sfxJump"+string(jump_count)));
    jump_count++;
}
if kr[ki.jump] and vv*grav_dir < 0 {
    vv /= 2;
}
//show_debug_message("vv="+string(vv));


//show_debug_message("Handle Ghost and Gravity");
if kp[ki.ghost] and global.ghost_upgrade {
    ghost = !ghost;
    if ghost { 
        sfx_play(sfxGhostOn);
    } else {
        sfx_play(sfxGhostOff);
    }
    with oB16 {
        solid = !other.ghost;
    }
}

grav_dir_previous = grav_dir;
if kp[ki.grav] and global.grav_upgrade and on_ground() {
    grav_dir = -grav_dir;
    if grav_dir == -1 {
        sfx_play(sfxGravOn);
    } else {
        sfx_play(sfxGravOff);
    }
}

//show_debug_message("Handle checkpointing");
if on_ground() and kp[ki.down] {
    checkpoint = true;
    checkpoint_x = x;
    checkpoint_y = y;
    checkpoint_xr = xr;
    checkpoint_yr = yr;
    checkpoint_grav = grav_dir;
    checkpoint_hdir = image_xscale;
    sfx_play(sfxCheckpoint);
}

//show_debug_message("Move character");
if !hmove(hv) {
    hv = 0;
}
if !vmove(vv) {
    if sign(vv) = grav_dir and abs(vv) > 4*grav {
        sfx_play(sfxLand);
    }
    vv = 0;
}
//show_debug_message("x="+string(x)+" y="+string(y));


//show_debug_message("Set sprite");
if hdir != 0 {
    image_xscale = hdir;
}
image_yscale = grav_dir;

if ghost {
    image_speed = default_image_speed;
    sprite_index = sG;
} else if !on_ground() and vv*grav_dir < 0 {
    image_speed = jump_image_speed;
    sprite_index = sJ;
} else if hdir != 0 {
    image_speed = default_image_speed;
    sprite_index = sR;
} else {
    image_speed = default_image_speed;
    sprite_index = sS;
}
//show_debug_message("Done Step!");

checkpoint_xr_previous = checkpoint_xr;
checkpoint_yr_previous = checkpoint_yr;

