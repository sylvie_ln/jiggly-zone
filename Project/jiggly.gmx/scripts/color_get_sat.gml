var h = color_get_hue(argument[0])/255;
var s = color_get_saturation(argument[0])/255;
var v = color_get_value(argument[0])/255;
var hh,ss,ll;
hh=h;
ll=(2-s)*v;
ss=s*v;
if ll <= 1 {
    ss/=ll;
} else {
    ss/=(2-ll);
}
ll/=2;
return round(ss*255);
