uses_surface = false;

acc = 0.3;
fric = 0.3;
grav = 0.1;

i=0;
jmp[i++] = sqrt(2*68*grav);
jmp[i++] = sqrt(2*52*grav);
jmp[i++] = sqrt(2*36*grav);
jmp[i++] = sqrt(2*20*grav);
jmp[i++] = sqrt(2*12*grav);
jmp[i++] = sqrt(2*8*grav);
jump_count = 0;
jump_max = i;

hmax = 2;
vmax = 8;

hv = 0;
vv = 0;

image_xscale=-1;
hdir = 0;
grav_dir = 1;

xr = 0;
yr = 0;

ghost = false;

default_image_speed = 10/room_speed;
jump_image_speed = 20/room_speed;

last_door = noone;
dead = false;
death_timer = 0;
death_time = room_speed div 2;
explode_time = death_time div 1;

jump_to_door = -1;
jumped_to_door = false;
door_timer = 0;
door_time = room_speed div 2;
door_room = "";

checkpoint = false;
checkpoint_hdir = image_xscale;
checkpoint_x = x;
checkpoint_y = y;
checkpoint_xr = xr;
checkpoint_yr = yr;
checkpoint_xr_previous = xr;
checkpoint_yr_previous = yr;
checkpoint_grav = grav_dir;
grav_dir_previous = grav_dir;
pressed_retry = false;
