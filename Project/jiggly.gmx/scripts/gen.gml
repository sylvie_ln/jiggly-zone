var style = argument[0];
var object = argument[1];
if style == styles.adjacent_or_randomly
or style == styles.adjacent {
    var target = argument[2];
}

rectangle_factor = params[styles.rectangles,rect_params.rectangle_factor];
rectangle_step_factor = params[styles.rectangles,rect_params.rectangle_step_factor];
min_width = params[styles.rectangles,rect_params.min_width];
min_height = params[styles.rectangles,rect_params.min_height];
max_width = params[styles.rectangles,rect_params.max_width];
max_height = params[styles.rectangles,rect_params.max_height];
min_total = params[styles.rectangles,rect_params.min_total];
max_total = params[styles.rectangles,rect_params.max_total];
border_distance = params[styles.rectangles,rect_params.border_distance];
hollow = params[styles.rectangles,rect_params.hollow];
overlap_allowed = params[styles.rectangles,rect_params.overlap_allowed];
max_rectangle_steps = params[styles.rectangles,rect_params.max_rectangle_steps];
max_rectangle_attempts = params[styles.rectangles,rect_params.max_rectangle_attempts];

adjacent_factor = params[styles.adjacent,adja_params.adjacent_factor];
adjacent_count_max = params[styles.adjacent,adja_params.adjacent_count_max];
check_diagonals = params[styles.adjacent,adja_params.check_diagonals];
direction_memory = params[styles.adjacent,adja_params.direction_memory];
direction_memory_probability = params[styles.adjacent,adja_params.direction_memory_probability];
direction_locks = params[styles.adjacent,adja_params.direction_locks];
adjacent_attempts = params[styles.adjacent,adja_params.adjacent_attempts];


random_factor = params[styles.randomly,rand_params.random_factor];
random_attempts = params[styles.randomly,rand_params.random_attempts];

adjacent_probability = params[styles.adjacent_or_randomly,ajra_params.adjacent_probability];

switch(style) {
    case styles.rectangles:        
        var usable_blocks = floor(total_blocks*rectangle_factor);
        usable_blocks = max(usable_blocks,1);
        var used_blocks = 0;
        var steps = 0;
        while used_blocks < usable_blocks and steps <= max_rectangle_steps {
            steps++;
            var usable_this_step = max(floor(rectangle_step_factor*(usable_blocks-used_blocks)),1);
            if usable_this_step == 1 {
                var w = 1;
                var h = 1;
                if min_width * min_height > 1 {
                    break;
                }
            } else {
                var attempts = 0;
                while true {
                    attempts++;
                    var w = irandom_range(min_width,min(usable_this_step,area_width div block_size,max_width));
                    var h = irandom_range(min_height,min(usable_this_step,area_height div block_size,max_height));
                    if (w*h <= usable_this_step and w*h <= max_total) or attempts > max_rectangle_attempts {
                        break;
                    }
                }
                if attempts > max_rectangle_attempts {
                    continue;
                }
            }
            repeat(max_rectangle_attempts) {
                var xp = max(irandom(area_width  div grid_size)-(w*(block_size div grid_size)),0) * grid_size;
                var yp = max(irandom(area_height div grid_size)-(h*(block_size div grid_size)),0) * grid_size; 
                if overlap_allowed or !collision_rectangle(xp-border_distance,yp-border_distance,xp+w*16+border_distance,yp+h*16+border_distance,oLevelObject,false,false) {
                    for(i=0;i<w;i++) {
                        for(j=0;j<h;j++) {
                            if !hollow or i=0 or j=0 or i=w-1 or j=h-1 {
                                if !pm(xp+i*block_size,yp+j*block_size,oLevelObject) {
                                    var b = instance_create(xp+i*block_size,yp+j*block_size,object);
                                }
                            }
                        }
                    }
                    if hollow {
                        used_blocks += 2*w +2*(h-1);
                    } else {
                        used_blocks += w*h;
                    }
                    break;
                }
            }
        }
    break;
    case styles.adjacent:
        var usable_blocks = floor(instance_number(target)*adjacent_factor);
        usable_blocks = max(usable_blocks,1);
        repeat(usable_blocks) {
            gen_adjacent_one(object,target);
        }
    break;
    case styles.randomly:
        var usable_blocks = floor((total_blocks-instance_number(oLevelObject))*random_factor);
        usable_blocks = max(usable_blocks,1);
        repeat(usable_blocks) {
            gen_random_one(object);
        }
    break;
    case styles.adjacent_or_randomly:
        var usable_blocks = 
            floor(instance_number(target)*adjacent_factor*adjacent_probability 
                 +(total_blocks-instance_number(oLevelObject))*random_factor*(1-adjacent_probability));
        usable_blocks = max(usable_blocks,1);
        repeat(usable_blocks) {
            if random(1) < adjacent_probability {
                gen_adjacent_one(object,target);
            } else {
                gen_random_one(object);
            }
        }
    break;
}
