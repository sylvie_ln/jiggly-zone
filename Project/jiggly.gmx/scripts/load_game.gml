var prefix = "";
if global.second_quest {
    prefix = "2Q";
}
ini_open(save_file_name());
//show_debug_message("Changing rooms");
var col = ini_read_string("room","color","pink");
var num = ini_read_string("room","number","2");
var alt = ini_read_string("room","alt","");
var rm = col+num+alt;

//show_debug_message("Ending check");
global.ending_done = ini_read_real("end","done",false);

//show_debug_message("Create character");
var xp = ini_read_real("char","x",global.char_start_x);
var yp = ini_read_real("char","y",global.char_start_y);
var xr = ini_read_real("char","xr",0);
var yr = ini_read_real("char","yr",0);
var grav = ini_read_real("char","grav",1);
var ghost = ini_read_real("char","ghost",0);

//show_debug_message("Set treasures");
for(i=0;i<sprite_get_number(sTreasure);i++) {
    global.treasure[i] = ini_read_real("treasure",string(i),false);
}
set_upgrade_vars();
//show_debug_message("Set medals");
for(i=0;i<global.medal_total;i++) {
    for(j=0;j<global.shard_total;j++) {
        global.medal[i,j] = ini_read_real("medal",string(i)+"_"+string(j),false);
    }
}

//show_debug_message("Load maps");
if running_in_browser() {
    if file_exists(q2prefix()+"room_explored.json") {
        var f = file_text_open_read(q2prefix()+"room_explored.json");
        ds_map_read(room_explored,file_text_read_string(f));
        file_text_close(f);
    }
    if file_exists(q2prefix()+"chest_opened.json") {
        var f = file_text_open_read(q2prefix()+"chest_opened.json");
        ds_map_read(chest_opened,file_text_read_string(f));
        file_text_close(f);
    }
    if file_exists(q2prefix()+"npc_talked.json") {
        var f = file_text_open_read(q2prefix()+"npc_talked.json");
        ds_map_read(npc_talked,file_text_read_string(f));
        file_text_close(f);
    }
    if file_exists(q2prefix()+"door_entered.json") {
        var f = file_text_open_read(q2prefix()+"door_entered.json");
        ds_map_read(door_entered,file_text_read_string(f));
        file_text_close(f);
    }
} else {
    var re = ini_read_string("room_explored","data","");
    if re != "" {
        ds_map_read(room_explored,re);
    }
    var co = ini_read_string("chest_opened","data","");
    if co != "" {
        ds_map_read(chest_opened,co);
    }
    var nt = ini_read_string("npc_talked","data","");
    if nt != "" {
        ds_map_read(npc_talked,nt);
    }
    var de = ini_read_string("door_entered","data","");
    if de != "" {
        ds_map_read(door_entered,de);
    }
}
//show_debug_message("Loading done");
ini_close();

if global.ending_done {
    instance_create(0,0,oEnding);
    room_go(rm);
} else {
    if !global.edit {
        instance_create(xp,yp,oChar);
        with oChar { platform_init(); }
        oChar.xr = 0; //xr;
        oChar.yr = 0; //yr;
        oChar.grav_dir = 1; //grav;
        oChar.ghost = false; //ghost;
        //oChar.checkpoint_grav = oChar.grav_dir;
    }
    room_go(rm);
}

