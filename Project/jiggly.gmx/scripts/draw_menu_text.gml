var xp = argument[0];
var yp = argument[1];
var op = argument[2];
var sub_op = -1;
var txt = "";
var selected = false;
var alt = false;

if argument_count == 3 {
    txt = option_text[op];
    selected = (option == op);
    alt = (mode != modes.main);
    draw_set_font(get_font(font_main,font_main_size,"B"));
}

if argument_count == 4 {
    sub_op = argument[3];
    txt = sub_option_text[sub_op];
    selected = (sub_option[op] == sub_op);
    alt = (mode != modes.sub) or (option != op);
    if sub_option_type[sub_op] == sub_types.multi { 
        if sub_op == sub_options.scaling_x {
            var val = sub_option_value[sub_op];
            txt = string_format(val,(val div 10)+1,1)+txt;
        } else if sub_op == sub_options.key_repeat_delay_adjust
               or sub_op == sub_options.key_repeat_speed_adjust {
            var val = sub_option_value[sub_op];
            txt = string_format(round(val),floor(log10(val))+1,0)+txt;
        } else {
            txt = string(sub_option_value[sub_op])+txt;
        }
        
    }
    draw_set_font(get_font(font_main,font_main_size));
}

if selected {
    if alt {
        draw_alt_selected_text(xp,yp,txt);
    } else {
        draw_selected_text(xp,yp,txt);
    }
} else {
    draw_unselected_text(xp,yp,txt);
}

return txt;

