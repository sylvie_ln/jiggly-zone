var hh,ss,ll;
var hh = argument[0]/240;
var ss = argument[1]/240;
var ll = argument[2]/240;
var h,s,v;
h = hh;
ll *= 2;
if ll <= 1 {
    ss *= ll;
} else {
    ss *= 2-ll;
}
v = (ll+ss)/2;
s = (2*ss)/(ll+ss);
return make_color_hsv(round(h*255),round(s*255),round(v*255));
