// what to save?
// room color/number/alt
// position in room
// treasures
// medallions
// rooms explored
// chests open/close status
// jigglers spoken to

// must assign id numbers to chests and jigglers upon creation in a deterministic way
ini_open(save_file_name());
ini_write_real("end","done",global.ending_done);
ini_write_string("room","color",room_get_color(room));
ini_write_string("room","number",string(room_get_number(room)));
if room_get_alt(room) {
    ini_write_string("room","alt","a");
} else {
    ini_write_string("room","alt","");
}
if instance_exists(oChar) {
    var door = oChar.last_door;
    if !instance_exists(door) {
        ini_write_real("char","x",oChar.xstart);
        ini_write_real("char","y",oChar.ystart);
    } else {
        ini_write_real("char","x",door.x);
        ini_write_real("char","y",door.y);
    }
    ini_write_real("char","xr",oChar.xr);
    ini_write_real("char","yr",oChar.yr);
    ini_write_real("char","grav",oChar.grav_dir);
    ini_write_real("char","ghost",oChar.ghost);
} else {
    ini_write_real("char","x",global.char_start_x);
    ini_write_real("char","y",global.char_start_y);
    ini_write_real("char","xr",0);
    ini_write_real("char","yr",0);
    ini_write_real("char","grav",1);
    ini_write_real("char","ghost",0);
}
for(i=0;i<sprite_get_number(sTreasure);i++) {
    ini_write_real("treasure",string(i),global.treasure[i]);
}
for(i=0;i<global.medal_total;i++) {
    for(j=0;j<global.shard_total;j++) {
        ini_write_real("medal",string(i)+"_"+string(j),global.medal[i,j]);
    }
}
if running_in_browser() {
    var f = file_text_open_write(q2prefix()+"room_explored.json");
    file_text_write_string(f,ds_map_write(room_explored));
    file_text_close(f);
    var f = file_text_open_write(q2prefix()+"chest_opened.json");
    file_text_write_string(f,ds_map_write(chest_opened));
    file_text_close(f);
    var f = file_text_open_write(q2prefix()+"npc_talked.json");
    file_text_write_string(f,ds_map_write(npc_talked));
    file_text_close(f);
    var f = file_text_open_write(q2prefix()+"door_entered.json");
    file_text_write_string(f,ds_map_write(door_entered));
    file_text_close(f);
} else {
    ini_write_string("room_explored","data",ds_map_write(room_explored));
    ini_write_string("chest_opened","data",ds_map_write(chest_opened));
    ini_write_string("npc_talked","data",ds_map_write(npc_talked));
    ini_write_string("door_entered","data",ds_map_write(door_entered));
}
ini_close();

