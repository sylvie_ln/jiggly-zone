var pam = argument[2];
var i = argument[1];
with argument[0] {
    for(var j=0;j<params_last[0];j++) {
        if params_type[0,j] == clickable_types.text and params_arg[0,j] != true {
            pam[@ i,j] = ini_read_string("object_"+string(i),"param_"+string(j),params[0,j]);
        } else {
            pam[@ i,j] = ini_read_real("object_"+string(i),"param_"+string(j),params[0,j]);
        }
    }
}
return pam;
