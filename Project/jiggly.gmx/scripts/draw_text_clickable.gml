var hpos = argument[0];
var vpos = argument[1];
var txt = argument[2];
draw_text(hpos,vpos,txt);
var chpos = hpos - view_xview[0];
var cvpos = vpos - view_yview[0];
var c = ip(chpos,cvpos,oClickable);
if c == noone {
    var c = instance_create(chpos,cvpos,oClickable);
}
c.w = string_width(txt);
c.h = string_height(txt);
c.type = argument[3];
c.focus = argument[4];
c.style = argument[5];
c.param = argument[6];
c.active = 2;

switch(c.type) {
    case clickable_types.loop:
        c.last = argument[7];
    break;
    case clickable_types.add:
        c.delta = argument[7];
    break;
    case clickable_types.text:
        c.is_num = argument[7];
    break;
}
