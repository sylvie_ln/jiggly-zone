var amount = argument[0];
var move = round(amount+yr);
yr += amount-move;

var rval = true;

while move != 0 {
    var s = sign(move);
    
    if pfr(0,s) {
        y += s;
        move -= s;
    } else {
        rval = false;
        break;
    }
}

return rval;
