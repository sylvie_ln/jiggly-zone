var name = room_names[? argument[0]];
return string_char_at(name,string_length(name)) == 'a';
