enum gamepad_types { xbox, ps, unknown }
var g = argument0;
var gamepad_desc = string_upper(gamepad_get_description(gpa_id(g)));
if string_pos("XBOX",gamepad_desc) != 0 {
    return gamepad_types.xbox;
}
if string_pos("PLAYSTATION",gamepad_desc) != 0 or string_pos("DUALSHOCK",gamepad_desc) != 0 {
    return gamepad_types.ps;
}
return gamepad_types.unknown;

