var xp = argument[0];
var yp = argument[1];
var txt = argument[2];
if argument_count == 4 {
    var col = argument[3];
    draw_set_color(col);
} else {
    draw_set_color(c_fuchsia);
}
draw_rectangle(xp-3,yp-1,xp+string_width(txt)+1,yp+string_height(txt)+1,true);
draw_text(xp,yp+1,txt);
draw_text(xp,yp-1,txt);
draw_text(xp+1,yp,txt);
draw_text(xp-1,yp,txt);
draw_set_color(c_white);
draw_text(xp,yp,txt);
