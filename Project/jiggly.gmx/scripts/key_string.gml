if is_array(argument0) {
    return gpa_string(argument0);
}
switch(argument0)
{
    case vk_add: return "Add"
    case vk_alt: return "Alt"
    case vk_backspace: return "Backspace"
    case vk_control: return "Ctrl"
    case vk_decimal: return "Decimal"
    case vk_delete: return "Delete"
    case vk_divide: return "Divide"
    case vk_down: return "Down"
    case vk_end: return "End"
    case vk_enter: return "Enter"
    case vk_escape: return "Escape"
    case vk_f1 : return "F1"
    case vk_f10: return "F10"
    case vk_f11: return "F11"
    case vk_f12: return "F12"
    case vk_f2: return "F2"
    case vk_f3: return "F3"
    case vk_f4: return "F4"
    case vk_f5: return "F5"
    case vk_f6: return "F6"
    case vk_f7: return "F7"
    case vk_f8: return "F8"
    case vk_f9: return "F9"
    case vk_home: return "Home"
    case vk_insert: return "Insert"
    case vk_lalt: return "LAlt"
    case vk_lcontrol: return "LCtrl"
    case vk_left: return "Left"
    case vk_lshift: return "LShift"
    case vk_multiply: return "Multiply"
    case vk_numpad0: return "Num 0"
    case vk_numpad1: return "Num 1"
    case vk_numpad2: return "Num 2"
    case vk_numpad3: return "Num 3"
    case vk_numpad4: return "Num 4"
    case vk_numpad5: return "Num 5"
    case vk_numpad6: return "Num 6"
    case vk_numpad7: return "Num 7"
    case vk_numpad8: return "Num 8"
    case vk_numpad9: return "Num 9"
    case vk_pagedown: return "PgDn"
    case vk_pageup: return "PgUp"
    case vk_pause: return "Pause"
    case vk_printscreen: return "PrtScr"
    case vk_ralt: return "RAlt"
    case vk_rcontrol: return "RCtrl"
    //case vk_return: return "Return"
    case vk_right: return "Right"
    case vk_rshift: return "RShift"
    case vk_shift: return "Shift"
    case vk_space: return "Space"
    case vk_subtract: return "Subtract"
    case vk_tab: return "Tab"
    case vk_up: return "Up"
    case 186: case 59: return "; :";
    case 187: case 61: return "= +";
    case 188: return ", <";
    case 189: case 173: return "- _";
    case 190: return ". >";
    case 191: return "? /";
    case 192: return "` ~";
    case 219: return "[ {";
    case 220: return "\ |";
    case 221: return "] }";
    case 222: return "'"+' "';
    case 20: return "Caps Lock";
    case 144: return "Num Lock";
    case oInput.unpressable_key: return "Not Set";
    default: return string_upper(chr(argument0));
    /**/
}
