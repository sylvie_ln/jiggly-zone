random_set_seed(0);

with oLevelObject {
    if object_index == oChar { continue; }
    instance_destroy();
}

var level = ds_list_create();
var rm = room_names[? room];
ini_open(rm+".lv");
var version = ini_read_real("meta","version",0);
var ld = ini_read_string(rm,"data","");
if ld != "" {
    ds_list_read(level,ld);
}
with oLevelGen {
    for(i=0;i<styles.last;i++) {
        for(j=0;j<params_last[i];j++) {
            params[i,j] = ini_read_real("level_gen","style"+string(i)+"param"+string(j),params[i,j]);
        }
    }
    style = ini_read_real("level_gen","style",style);
    object = ini_read_real("level_gen","object",object);
    target = ini_read_real("level_gen","target",target);
}
ini_close();
for(var i=0;i<ds_list_size(level);i+=0) {
    if version == 1 {
        var obj = level[|i++];
        if obj == oLevelGen {
            obj = oNPC;
        }
    } else {
        var obj = asset_get_index(level[|i++]);
        if obj == oLevelGen {
            obj = oNPC;
        }
    }
    var xp  = level[|i++];
    var yp  = level[|i++];
    var inst = instance_create(xp,yp,obj);
    if obj == oDoor 
    or obj == oChest 
    or obj == oNPC {
        with inst {
            var offset = 0; 
            if version == 2 {
                if obj == oNPC {
                    offset = 1;
                }
            }
            for(var j=0;j<params_last[0]-offset;j++) {
                params[0,j] = level[|i++];
            }
        }
    }
    with inst { event_user(7); }
}
/*
var id_str = ""
with oNPC {
    id_str += string(params[0,npc_params.num])+" ";
}
show_message(id_str);
*/
ds_list_destroy(level);
