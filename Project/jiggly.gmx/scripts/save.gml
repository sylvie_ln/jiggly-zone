var level = ds_list_create();
with oLevelObject {
    if object_index == oChar { continue; }
    if object_index == oChest and params[0,chest_params.num] == 999 {
        continue;
    }
    if object_index == oNPC and params[0,npc_params.num] == 999 {
        continue;
    }
    ds_list_add(level,object_get_name(object_index));
    ds_list_add(level,x);
    ds_list_add(level,y);
    if object_index == oDoor 
    or object_index == oChest
    or object_index == oNPC {
        for(var j=0;j<params_last[0];j++) {
            ds_list_add(level,params[0,j]);
        }
    }
}
var rm = room_names[? room];
ini_open(rm+".lv");
ini_write_real("meta","version",3);
ini_write_string(rm,"data",ds_list_write(level));
with oLevelGen {
    for(var i=0;i<styles.last;i++) {
        for(var j=0;j<params_last[i];j++) {
            ini_write_real("level_gen","style"+string(i)+"param"+string(j),params[i,j]);
        }
    }
    ini_write_real("level_gen","style",style);
    ini_write_real("level_gen","object",object);
    ini_write_real("level_gen","target",target);
}
ini_close();
ds_list_destroy(level);
