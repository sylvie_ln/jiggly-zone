var ears,hair,eyes,mouth,hands,feet;
var c_base,c_ears,c_hair,c_eyes,c_mouth,c_hands,c_feet;
var cp; // cute probability
var main_color, alt_color, eye_color;
var col = room_get_color(room);
if argument_count > 0 {
    col = argument[0];
}   

switch(col) {
    case "pink":
        cp[npc_params.ears] = 1;
        cp[npc_params.hair] = 1;
        cp[npc_params.eyes] = 1;
        cp[npc_params.mouth] = 1;
        cp[npc_params.hands] = 1;
        cp[npc_params.feet] = 1;
        main_color = 11;
        alt_color = irandom(rainbow-1); 
        ear_color = choose(main_color,alt_color);
        hair_color = choose(main_color,alt_color);
        eye_color = irandom(rainbow-1);
        hand_color = choose(main_color,alt_color);
        feet_color = choose(main_color,alt_color);
    break;
    case "red":
        cp[npc_params.ears] = 0.5;
        cp[npc_params.hair] = 0.5;
        cp[npc_params.eyes] = 0.5;
        cp[npc_params.mouth] = 0.5;
        cp[npc_params.hands] = 0.5;
        cp[npc_params.feet] = 0.5;
        main_color = choose(0,rainbow-1);
        alt_color = irandom(rainbow-1);
        ear_color = irandom(rainbow-1);
        hair_color = irandom(rainbow-1);
        eye_color = irandom(rainbow-1);
        hand_color = irandom(rainbow-1);
        feet_color = irandom(rainbow-1);
    break;
    case "orange":
        cp[npc_params.ears] = 0.5;
        cp[npc_params.hair] = 0.5;
        cp[npc_params.eyes] = 1;
        cp[npc_params.mouth] = 0.5;
        cp[npc_params.hands] = 1;
        cp[npc_params.feet] = 1;
        main_color = 1;
        alt_color = irandom(rainbow-1); 
        ear_color = choose(main_color,alt_color);
        hair_color = choose(main_color,alt_color);
        eye_color = irandom(rainbow-1);
        hand_color = choose(main_color,alt_color);
        feet_color = choose(main_color,alt_color);
    break;
    case "yellow":        
        cp[npc_params.ears] = 0.5;
        cp[npc_params.hair] = 0.5;
        cp[npc_params.eyes] = 1;
        cp[npc_params.mouth] = 0.5;
        cp[npc_params.hands] = 1;
        cp[npc_params.feet] = 1;
        main_color = 2; 
        alt_color = irandom(rainbow-1); 
        ear_color = choose(main_color,alt_color);
        hair_color = choose(main_color,alt_color);
        eye_color = irandom(rainbow-1);
        hand_color = choose(main_color,alt_color);
        feet_color = choose(main_color,alt_color);
    break;
    case "green":        
        cp[npc_params.ears] = 0.5;
        cp[npc_params.hair] = 0.5;
        cp[npc_params.eyes] = 1;
        cp[npc_params.mouth] = 0.5;
        cp[npc_params.hands] = 1;
        cp[npc_params.feet] = 1;
        main_color = (4+irandom_range(-1,1)) mod rainbow; 
        alt_color = irandom(rainbow-1); 
        ear_color = choose(main_color,alt_color);
        hair_color = choose(main_color,alt_color);
        eye_color = irandom(rainbow-1);
        hand_color = choose(main_color,alt_color);
        feet_color = choose(main_color,alt_color);
    break;
    case "blue":        
        cp[npc_params.ears] = 0.5;
        cp[npc_params.hair] = 0.5;
        cp[npc_params.eyes] = 1;
        cp[npc_params.mouth] = 0.5;
        cp[npc_params.hands] = 0.5;
        cp[npc_params.feet] = 1;
        main_color = (7+irandom_range(-1,1)) mod rainbow; 
        alt_color = irandom(rainbow-1); 
        ear_color = irandom(rainbow-1);
        hair_color = choose(main_color,alt_color);
        eye_color = irandom(rainbow-1);
        hand_color = irandom(rainbow-1);
        feet_color = choose(main_color,alt_color);
    break;
    case "purple":        
        cp[npc_params.ears] = 0.5;
        cp[npc_params.hair] = 0.5;
        cp[npc_params.eyes] = 1;
        cp[npc_params.mouth] = 0.5;
        cp[npc_params.hands] = 1;
        cp[npc_params.feet] = 0.5;
        main_color = (9+irandom_range(0,1)) mod rainbow; 
        alt_color = irandom(rainbow-1); 
        ear_color = irandom(rainbow-1);
        hair_color = choose(main_color,alt_color);
        eye_color = irandom(rainbow-1);
        hand_color = choose(main_color,alt_color);
        feet_color = irandom(rainbow-1);
    break;
}

if room_get_alt(room) {
    cp[npc_params.ears] = 0.5;
    cp[npc_params.hair] = 0.5;
    cp[npc_params.eyes] = 0.5;
    cp[npc_params.mouth] = 0.5;
    cp[npc_params.hands] = 0.5;
    cp[npc_params.feet] = 0.5;
    main_color = irandom(rainbow-1);
}

c_base = main_color;
c_ears = ear_color
c_hair = hair_color;
c_eyes = eye_color;
c_mouth = c_base;
c_hands = hand_color;
c_feet = feet_color;

if random(1) <= cp[npc_params.ears] {
    ears = choose(1,2,5,8,9,11,12,15);
} else {
    ears = choose(3,4,6,7,10,13,14);
}
if random(1) <= cp[npc_params.hair] {
    hair = choose(1,2,5,6,7,12,13,14);
} else {
    hair = choose(3,4,8,9,10,11,15);
}
if random(1) <= cp[npc_params.eyes] {
    eyes = choose(1,4,6,8,9,11,12,13,14,15);
} else {
    eyes = choose(2,3,5,7,10,16);
}
if random(1) <= cp[npc_params.mouth] {
    mouth = choose(1,2,3,4,5,6,9);
} else {
    mouth = choose(7,8,10,11,12,13,14,15);
}
if random(1) <= cp[npc_params.hands] {
    hands = choose(1,2,3,5);
} else {
    hands = choose(4,6,7,8);
}
if random(1) <= cp[npc_params.feet] {
    feet = choose(1,2,4);
} else {
    feet = choose(3,5,6,7,8);
}
ears--;
hair--;
eyes--;
mouth--;
hands--;
feet--;

params[0,npc_params.ears] = ears;
params[0,npc_params.hair] = hair;
params[0,npc_params.eyes] = eyes;
params[0,npc_params.mouth] = mouth;
params[0,npc_params.hands] = hands;
params[0,npc_params.feet] = feet;
params[0,npc_params.c_base] = c_base;
params[0,npc_params.c_ears] = c_ears;
params[0,npc_params.c_hair] = c_hair;
params[0,npc_params.c_eyes] = c_eyes;
params[0,npc_params.c_mouth] = c_mouth;
params[0,npc_params.c_hands] = c_hands;
params[0,npc_params.c_feet] = c_feet;
