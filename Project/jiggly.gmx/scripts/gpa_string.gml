var g = argument0;
if gpa_name(g) != "" {
    return gpa_name(g);
}
switch(gpa_button(g)) {
    case gp_axislh: 
        if gpa_axis_dir(g) > 0 {
            return "Right";
        } else if gpa_axis_dir(g) < 0 {
            return "Left";
        } else {
            return "Neutral";
        } 
    case gp_axislv:
        if gpa_axis_dir(g) > 0 {
            return "Down";
        } else if gpa_axis_dir(g) < 0 {
            return "Up";
        } else {
            return "Neutral";
        } 
    case gp_axisrh:
        if gpa_axis_dir(g) > 0 {
            return "Right";
        } else if gpa_axis_dir(g) < 0 {
            return "Left";
        } else {
            return "Neutral";
        } 
    case gp_axisrv:
        if gpa_axis_dir(g) > 0 {
            return "Down";
        } else if gpa_axis_dir(g) < 0 {
            return "Up";
        } else {
            return "Neutral";
        } 
    case gp_face1:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "A";
        }
        return "Button 1";
    case gp_face2:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "B";
        }
        return "Button 2";
    case gp_face3:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "X";
        }
        return "Button 3";
    case gp_face4:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "Y";
        }
        return "Button 4";
    case gp_padd:
        return "Down";
    case gp_padl:
        return "Left";
    case gp_padr:
        return "Right";
    case gp_padu:
        return "Up";
    case gp_shoulderl:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "LB";
        }
        return "Button 5";
    case gp_shoulderlb:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "LT";
        }
        return "Button 6";
    case gp_shoulderr:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "RB";
        }
        return "Button 7";
    case gp_shoulderrb:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "RT";
        }
        return "Button 8";
    case gp_start:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "Start";
        }
        return "Button 9";
    case gp_select:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "Back";
        }
        return "Button 10";
    case gp_stickl:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "LStick";
        }
        return "Button 11";
    case gp_stickr:
        switch(gpa_type(g)) {
            case gamepad_types.xbox: return "RStick";
        }
        return "Button 12";
    default:
        return "Unknown Gamepad Button";
}

