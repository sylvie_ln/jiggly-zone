if keyboard_check_pressed(vk_anykey) { 
    gamepad_array = gamepad_array_create(-1,-1,0,"");
    return true; 
}
var gamepad_button = -1;
var gamepad_axis_dir = 0;
for(i=0;i<gamepad_get_device_count();i++) {
    if gamepad_is_connected(i) {
        gamepad_set_axis_deadzone(i,global.deadzone);
        if gamepad_button_check_pressed(i,gp_face1) { gamepad_button = gp_face1; }
        if gamepad_button_check_pressed(i,gp_face2) { gamepad_button = gp_face2; }
        if gamepad_button_check_pressed(i,gp_face3) { gamepad_button = gp_face3; }
        if gamepad_button_check_pressed(i,gp_face4) { gamepad_button = gp_face4; }
        if gamepad_button_check_pressed(i,gp_padd) { gamepad_button = gp_padd; }
        if gamepad_button_check_pressed(i,gp_padr) { gamepad_button = gp_padr; }
        if gamepad_button_check_pressed(i,gp_padu) { gamepad_button = gp_padu; }
        if gamepad_button_check_pressed(i,gp_padl) { gamepad_button = gp_padl; }
        if gamepad_button_check_pressed(i,gp_select) { gamepad_button = gp_select; }
        if gamepad_button_check_pressed(i,gp_start) { gamepad_button = gp_start; }
        if gamepad_button_check_pressed(i,gp_shoulderl) { gamepad_button = gp_shoulderl; }
        if gamepad_button_check_pressed(i,gp_shoulderr) { gamepad_button = gp_shoulderr; }
        if gamepad_button_check_pressed(i,gp_shoulderlb) { gamepad_button = gp_shoulderlb; }
        if gamepad_button_check_pressed(i,gp_shoulderrb) { gamepad_button = gp_shoulderrb; }
        if gamepad_button_check_pressed(i,gp_stickr) { gamepad_button = gp_stickr; }
        if gamepad_button_check_pressed(i,gp_stickl) { gamepad_button = gp_stickl; }
        var previous;
        previous = oInput.previous_axis[i,axis.lh];
        if abs(gamepad_axis_value(i,gp_axislh)) > 0 and previous == 0 { gamepad_button = gp_axislh; gamepad_axis_dir = sign(gamepad_axis_value(i,gp_axislh)); }
        previous = oInput.previous_axis[i,axis.rh];
        if abs(gamepad_axis_value(i,gp_axisrh)) > 0 and previous == 0 { gamepad_button = gp_axisrh; gamepad_axis_dir = sign(gamepad_axis_value(i,gp_axisrh)); }
        previous = oInput.previous_axis[i,axis.lv];
        if abs(gamepad_axis_value(i,gp_axislv)) > 0 and previous == 0 { gamepad_button = gp_axislv; gamepad_axis_dir = sign(gamepad_axis_value(i,gp_axislv)); }
        previous = oInput.previous_axis[i,axis.rv];
        if abs(gamepad_axis_value(i,gp_axisrv)) > 0 and previous == 0 { gamepad_button = gp_axisrv; gamepad_axis_dir = sign(gamepad_axis_value(i,gp_axisrv)); }
        if gamepad_button != -1 { 
            gamepad_array = gamepad_array_create(i,gamepad_button,gamepad_axis_dir,""); 
            return true;
        }
    }
}    
return false;

