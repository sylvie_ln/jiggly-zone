var object = argument[0];
repeat(random_attempts) {
    var xp = irandom((area_width div grid_size)-1) * grid_size;
    var yp = irandom((area_height div grid_size)-1) * grid_size;
    if !pm(xp,yp,oLevelObject) {
        instance_create(xp,yp,object);
        break;
    } 
}
