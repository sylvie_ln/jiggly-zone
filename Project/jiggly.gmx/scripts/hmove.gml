var amount = argument[0];
var move = round(amount+xr);
xr += amount-move;

var rval = true;

while move != 0 {
    var s = sign(move);
    
    if pfr(s,0) {
        x += s;
        move -= s;
    } else {
        rval = false;
        break;
    }
}

return rval;

