var rm = room_names[? room];
file_delete(rm+".lv_html5");
ini_open(rm+".lv_html5");
var i = 0;
with oLevelObject {
    if object_index == oChar { continue; }
    if object_index == oChest and params[0,chest_params.num] == 999 {
        continue;
    }
    if object_index == oNPC and params[0,npc_params.num] == 999 {
        continue;
    }
    ini_write_string("object_"+string(i),"name",object_get_name(object_index));
    ini_write_real("object_"+string(i),"x",x);
    ini_write_real("object_"+string(i),"y",y);
    if object_index == oDoor 
    or object_index == oChest
    or object_index == oNPC {
        for(var j=0;j<params_last[0];j++) {
            if params_type[0,j] == clickable_types.text and params_arg[0,j] != true {
                ini_write_string("object_"+string(i),"param_"+string(j),params[0,j]);
            } else {
                ini_write_real("object_"+string(i),"param_"+string(j),params[0,j]);
            }
        }
    }
    i++;
}
ini_write_real("meta","version",3);
ini_write_real("meta","num_objects",i);
ini_close();
