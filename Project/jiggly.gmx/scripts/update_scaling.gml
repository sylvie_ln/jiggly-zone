var display_width = display_get_width();
var display_height = display_get_height();
smax = scaling_max();
switch(global.scaling) {
    case sub_options.scaling_auto:
        if global.scaling_x > smax {
            if global.interpolation {
                global.scaling_x = smax;
            } else {
                global.scaling_x = floor(smax);
            }
        }
    case sub_options.scaling_x:
        if global.scaling_x > smax {
            global.scaling_x = smax;
        }
        global.game_width = global.default_game_width*global.scaling_x;
        global.game_height = global.default_game_height*global.scaling_x;
    break;
    case sub_options.scaling_full_aspect:
        if display_width >= display_height {
            global.game_width = global.default_game_width*(display_height/global.default_game_height)
            global.game_height = display_height;
        } else {
            global.game_width = display_width;
            global.game_height = global.default_game_height*(display_width/global.default_game_width);
        }
    break;
    case sub_options.scaling_full:
        global.game_width = display_width
        global.game_height = display_height;
    break;
}
if !global.fullscreen {
    if running_in_browser() {
        window_set_size(display_width,display_height);
    } else {
        window_set_size(global.game_width,global.game_height);
    }
}

sub_option_value[sub_options.scaling_x] = global.scaling_x;
sub_option_max[sub_options.scaling_x] = smax;

alarm[0] = 1;
