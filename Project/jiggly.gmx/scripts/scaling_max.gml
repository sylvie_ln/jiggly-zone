var display_width = display_get_width();
var display_height = display_get_height();
var sc = 1;

while (global.default_game_width*sc < display_width)
and   (global.default_game_height*sc < display_height) {
    sc += sub_option_delta[sub_options.scaling_x];
}
sc -= sub_option_delta[sub_options.scaling_x];

return sc;
