var key = argument[0];
if is_array(key) {
    //var i = gpa_id(key);
    for(var i=0;i<gamepad_get_device_count();i++) {
        if !gamepad_is_connected(i) { continue; }
        var previous;
        switch(gpa_button(key)) {
            case gp_axislh:
            previous = oInput.previous_axis[i,axis.lh];
            if previous == 0 and sign(gamepad_axis_value(i,gpa_button(key))) == gpa_axis_dir(key) { return true; }
            break;
            case gp_axisrh:
            previous = oInput.previous_axis[i,axis.rh];
            if previous == 0 and sign(gamepad_axis_value(i,gpa_button(key))) == gpa_axis_dir(key) { return true; }
            break;
            case gp_axislv:
            previous = oInput.previous_axis[i,axis.lv];
            if previous == 0 and sign(gamepad_axis_value(i,gpa_button(key))) == gpa_axis_dir(key) { return true; }
            break;
            case gp_axisrv:
            previous = oInput.previous_axis[i,axis.rv];
            if previous == 0 and sign(gamepad_axis_value(i,gpa_button(key))) == gpa_axis_dir(key) { return true; }
            break;
        }
        if !is_axis_constant(gpa_button(key)) {
            if gamepad_button_check_pressed(i,gpa_button(key)) { return true; }
        }
    }
    return false;
} else {
    return keyboard_check_pressed(key);
}
