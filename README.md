# JIGGLY ZONE Source Code

This is a GameMaker Studio 1.4 project. You probably need GameMaker Studio 1.4 to open it properly - if you import it into GameMaker Studio 2 it will make a bunch of weird changes for compatibility purposes and I don't know if the game will work anymore (it might, I haven't tested it).

If you have questions about the source you can contact me on Twitter (twitter.com/sylviefluff).